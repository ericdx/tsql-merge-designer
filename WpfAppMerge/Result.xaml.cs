﻿using ClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfAppMerge
{
    /// <summary>
    /// Interaction logic for Result.xaml
    /// </summary>
    public partial class Result : Window
    {
        DataCollector _dc;
        public Result(DataCollector dc)
        {
            InitializeComponent();
            tbSql.Text = dc.sql;
            _dc = dc;
            _dc?.makeTestSqlAndSP();
        }

        private void CbTest_Checked(object sender, RoutedEventArgs e)
        {
            if (cbTest.IsChecked ?? false)
            {
                _dc?.makeTestSqlAndSP();
            } else
            {
                _dc.printScript();
            }
        }

        private void BtnScript_Click(object sender, RoutedEventArgs e)
        {
            tbSql.Text = _dc.printScript(test: true, delTarget: true,linked: linkedSrv.Text, make_SP: false);
        }

        private void BtnSP_Click(object sender, RoutedEventArgs e)
        {
           tbSql.Text =  _dc.printScript(test: true, delTarget: true,linked: linkedSrv.Text, make_SP: true);
        }

        private void BtnCopy_Click(object sender, RoutedEventArgs e)
        {
            Clipboard.SetText(tbSql.Text);
        }
    }
}
