﻿using System;
using System.Data.SqlClient;
using System.Windows.Controls;

namespace WpfAppMerge
{
    internal class Alert
    {
        internal static void Error(ListView lv, SqlException ex)
        {
            if (ex.Number == 258)
lv.Items.Add("unable to connect to DB !");
            lv.Items.Add(ex.Message);
        }
    }
}