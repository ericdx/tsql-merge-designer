﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonLibrary;

namespace ClassLibrary
{
    public class tRow
    {
        public string itemName { get;  set; }

    }

    public class rowCollection
    {
        public List<tRow> TableCollection(List<string> l, string text = null)
        {

            return l.Where(li => string.IsNullOrEmpty(text) || li.ContainsAnycase(text)).Select(li => new tRow { itemName = li }).ToList();
           
        }

      //  public List<tRow> item_rows = new List<tRow>();

    }
}
