﻿using System;
using System.Collections.Generic;

using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class DataCollector
    {
        public string db1 { get; set; }
        public string db2 { get; set; }
        public string tab1 { get; set; }
        public string tab2 { get; set; }
        string tab2test => $"{tab2}_testMerge";
        public List<TableDesc> tables1 { get; set; }
        public List<TableDesc> tables2 { get; set; }
        public List<string> fieldsSrc { get; set; }
        public List<string> fieldsTarget { get; set; } //all tab fields      
        public List<string> _keysSrc { get; set; }
        public List<string> _keysTarget { get; set; }
        public string sqlSP { get; set; }

        public void makeTestSqlAndSP()
        {
            printScript(test: true);
        }

        public List<string> savedConns()
        {
            using (compactModel cm = new compactModel())
            {
                return cm.connEntities.Select(c => c.Name).ToList();

            }
        }
        public string sql;
        public string printScript(bool test = false, bool delTarget = false,string linked = "",bool make_SP = false)
        {
var keys1 = _keysSrc; 
            var keys2 = _keysTarget;
            var flds1 = string.Join(",", fieldsSrc.Where(k => ! keys1.Contains(k)));
            int i = 0; 
            var stringBuild = new List<string>();
            
            foreach (var k1 in keys1)
            {
                stringBuild.Add($"_target_.{keys2[i++]} = _source_.{k1}");
            }
            var on = string.Join(" AND ", stringBuild); // compare fields

            stringBuild.Clear();
            i = 0;
            foreach (var f1 in fieldsSrc)
            {
                if (keys1.Contains(f1)) { i++; continue; }
                stringBuild.Add($"_target_.{fieldsTarget[i++]} = _source_.{f1}");
            }
            var set = string.Join(" , ", stringBuild);
            var insert = string.Join(" , ", fieldsTarget.Where(k => !keys1.Contains(k)));
            stringBuild.Clear();
            foreach (var f1 in fieldsSrc)
            {
                if (keys1.Contains(f1)) continue;
                stringBuild.Add($"_source_.{f1}");
            }
            var values = string.Join(" , ", stringBuild);
            sql = "";
            if (make_SP)
            {
                test = false;
                sql = "create proc merge as begin\n";
            }

            var del = delTarget ? "when not matched by source then delete" : "";
            if (test) sql = $"select top 0 * into {tab2test} from {tab2};\n go;\n";
            sql += $@"MERGE {(test ? tab2test : tab2)} AS _target_ 
            		        USING
            		        (
            		            SELECT {flds1} 	                                      
            		            FROM   {linked}{tab1}
            		        ) AS _source_
            		        ON (
            		            {on}
            		        )
            		        WHEN MATCHED THEN 
            		        UPDATE 
            		        SET    
                                    {set}
            		               WHEN NOT MATCHED THEN
            		        INSERT
                                   ({insert})
                             VALUES
                                   (
                                   {values}                                   
                                    ) {del} \n";
            if (make_SP)
                sql += "end";
            return sql;
        }

    }
}
