﻿using ClassLibrary;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using WpfLib;

namespace WpfAppMerge
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public List<tRow> dgTbl1_ItemsSource { get; private set; }

        DataLayer data, data2;
        DataCollector dc = new DataCollector();
        public MainWindow()
        {
            InitializeComponent();

            fillConns(conn1); fillConns(conn2);
            //  BtnTables_Click(null,null);            

        }
        void fillConns(ComboBox cb)
        {
            //cb.Items.Clear();
            dc.savedConns().ForEach(d => cb.Items.Add(d));
        }
        private void buttonFields_Click(object sender, RoutedEventArgs e)
        {
            viewTables = false;
            var tab1 = dgTbl1.SelectedItem as tRow;
            var tab2 = dgTbl2.SelectedItem as tRow;
            if (tab1 == null || tab2 == null)
            {
                logLv.Items.Add("select tables first!");
                return;
            }
            btnShowScript.IsEnabled = true;
            var table = tab1.itemName;
            var rows = data.getColumnsFor(table);
            dc.fieldsSrc = rows; dc.tab1 = table;
            tbFilter1.Text = tbFilter2.Text = "";
            dgTbl1.ItemsSource = rc.TableCollection(rows,tbFilter1.Text);
            var rows2 = data2.getColumnsFor(tab2.itemName);
            dc.fieldsTarget = rows2; dc.tab2 = tab2.itemName;

            dgTbl2.ItemsSource = rc.TableCollection(rows2,tbFilter2.Text);
        }
        void moveMice(DataGrid dg, MouseEventArgs e)
        {
            base.OnMouseMove(e);
            if (e.LeftButton == MouseButtonState.Pressed)
            {

                // Package the data.
                DataObject data = new DataObject();
                if (dg.SelectedItem == null) return;
                data.SetData(DataFormats.Text, dg.SelectedItem);

                // Inititate the drag-and-drop operation.
                DragDrop.DoDragDrop(this, data, DragDropEffects.Copy);
            }
        }
        private void DgTbl1_MouseMove(object sender, MouseEventArgs e)
        {
            moveMice(dgTbl1, e);
        }

        private void LbKeys_Drop(object sender, DragEventArgs e)
        {
            keysSrc.Items.Add(unpackCell(e));
        }
        string unpackCell(DragEventArgs e)
        {
            return ((tRow)e.Data.GetData(DataFormats.Text)).itemName;
        }

        private void KeysTarget_Drop(object sender, DragEventArgs e)
        {
            keysTarget.Items.Add(unpackCell(e));
        }

        private void DgTbl2_MouseMove(object sender, MouseEventArgs e)
        {
            moveMice(dgTbl2, e);
        }
        void moveUpDown(ListBox keysSrc, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
                keysSrc.Items.Remove(keysSrc.SelectedItem);
            else if (e.Key == Key.Oem6) // ] down
            {
                keysSrc.lbItemDown();
            }
            else
 if (e.Key == Key.Oem4) // [ up
            {
                keysSrc.lbItemUp();
            }
            else log($"pressed {e.Key}");
        }
        private void KeysSrc_KeyDown(object sender, KeyEventArgs e)
        {
            moveUpDown(sender as ListBox, e);
        }
        void log(string msg)
        {
            logLv.Items.Add(msg);
        }

        private void BtnShowScript_Click(object sender, RoutedEventArgs e)
        {

            if (keysSrc.Items.Count != keysTarget.Items.Count)
            {
                log("key fields count not match!");
                return;
            }
            dc._keysSrc = keysSrc.Items.OfType<string>().ToList();
            dc._keysTarget = keysTarget.Items.OfType<string>().ToList();
            dc.printScript(test: true,delTarget: true);
            Result wnd = new Result(dc);
            wnd.Show();
        }

        private void KeysTarget_KeyDown(object sender, KeyEventArgs e)
        {
            moveUpDown(sender as ListBox, e);
        }
        rowCollection rc = new rowCollection();

        public List<tRow> dgTbl2_ItemsSource { get; private set; }

        private void BtnPK_Click(object sender, RoutedEventArgs e)
        {
            if (data.keyFields.Count == 0) log("no PK in src table.");
            if (data2.keyFields.Count == 0) log("no PK in target table.");
            keysSrc.Items.Clear();
            keysTarget.Items.Clear();
            data.keyFields.ForEach(k => keysSrc.Items.Add(k));
            data2.keyFields.ForEach(k => keysTarget.Items.Add(k));
        }

        private void TbFilter1_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (viewTables)
                BtnTables_Click(null, null);
            else buttonFields_Click(null, null);
        }
        bool viewTables;

        /// <summary>
        /// shows list of objects, allows to select & rename with postfix
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnRenamer_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BtnTables_Click(object sender, RoutedEventArgs e)
        {
            viewTables = true;
            try
            {
                btnShowScript.IsEnabled = false;
                data = new DataLayer(conn1.Text);
                dc.db1 = data.dbName;
                var rows = data.getAllTables();
                dc.tables1 = data.tables;

                dgTbl1_ItemsSource = rc.TableCollection(rows,tbFilter1.Text);
                dgTbl1.ItemsSource = dgTbl1_ItemsSource;
                if (conn2.Text == "")
                    conn2.Text = conn1.Text;
                data2 = new DataLayer(conn2.Text);
                dc.db2 = data2.dbName;
                rows = data2.getAllTables();
                dc.tables2 = data2.tables;

                dgTbl2_ItemsSource = rc.TableCollection(rows, tbFilter2.Text);
                dgTbl2.ItemsSource = dgTbl2_ItemsSource;
                btnFields.IsEnabled = true;
                fillConns(conn1); fillConns(conn2);
                btnPK.IsEnabled = true;                
            }
            catch (Exception ex)
            {
                if (ex is SqlException)
                    Alert.Error(logLv, ex as SqlException);
            }
            finally
            {

            }
        }
    }

}
