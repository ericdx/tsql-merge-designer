﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace WpfLib
{
    public static class Helpers
    {
        static public void lbItemUp(this ListBox lb)
        {
            if (lb.SelectedIndex != -1) 
            {
                var selectedIndex = lb.SelectedIndex;
                if (selectedIndex - 1 >= 0)
                {
                    var itemToMoveUp = lb.SelectedItem;
                    lb.Items.RemoveAt(selectedIndex);
                    lb.Items.Insert(selectedIndex - 1, itemToMoveUp);
                    lb.SelectedIndex = selectedIndex - 1;
                }
            }

        }
        static public void lbItemDown( this ListBox lb)
        {
            if (lb.SelectedIndex != -1) 
            {
                var selectedIndex = lb.SelectedIndex;
                if (selectedIndex + 1 < lb.Items.Count)
                {
                    var itemToMoveDown = lb.SelectedItem;
                    lb.Items.RemoveAt(selectedIndex);
                    lb.Items.Insert(selectedIndex + 1, itemToMoveDown);
                    lb.SelectedIndex = selectedIndex + 1;
                }
            }
        }
    }
}
