﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class DataLayer
    {
        SqlConnection _conn;
        public string dbName => _conn.Database;

        public List<string> keyFields { get; private set; } = new List<string>();

        public DataLayer(string conn)
        {
            if (conn == "") { Logger.Error("connect to what?"); return; }
            _conn = new SqlConnection(conn);          
            try
            {

_conn.Open();
                saveConnection(conn);
            }
            catch (SqlException ex)
            {
                Logger.Error(ex.Message, ex);
               // throw;
            } 
            
        }

        private void saveConnection(string conn)
        {
            using (compactModel cm = new compactModel())
            {
                if (cm.connEntities.Find(conn) == null)
                    cm.connEntities.Add(new ConnectionEntity { Name = conn });
                cm.SaveChanges();
            }
        }
        List<string> read(string cmd1, int fldNum)
        {
 SqlCommand cmd = new SqlCommand(cmd1, _conn);
  if (_conn.State != System.Data.ConnectionState.Open) _conn.Open();
           var reader = cmd.ExecuteReader();
            List<string> rowsCollection = new List<string>();
            while (reader.Read())
            {                
                rowsCollection.Add(reader.GetString(fldNum));
            }
            reader.Close();
            return rowsCollection;
        }
        public List<string> getColumnsFor(string tbl)
        {
            keyFields = getPKfields(tbl);
            
            return read($"exec sp_columns '{tbl}'", 3);
            
        }
        public List<TableDesc> tables;
        
        List<string> getPKfields(string tbl)
        {
  return read($@"SELECT Col.Column_Name from 
    INFORMATION_SCHEMA.TABLE_CONSTRAINTS Tab,
    INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE Col
WHERE
    Col.Constraint_Name = Tab.Constraint_Name
    AND Col.Table_Name = Tab.Table_Name
    AND Constraint_Type = 'PRIMARY KEY'
    AND Col.Table_Name = '{tbl}'", 0);          
        }
        public List<string> getAllTables()
        {
             SqlCommand cmd = new SqlCommand($"exec sp_tables ", _conn);
            if (_conn.State != System.Data.ConnectionState.Open)
                _conn.Open();
            var reader = cmd.ExecuteReader();
            var rowsCollection = new List<string>();
            tables = new List<TableDesc>();
            while (reader.Read())
            {
                tables.Add(new TableDesc { schema = reader.GetString(1), name = reader.GetString(2) });
                rowsCollection.Add(reader.GetString(2));
            }            
            return rowsCollection;
        }
    }
}
